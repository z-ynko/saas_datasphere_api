package services

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/datasphere/src/database"
	"github.com/datasphere/src/util"
	"github.com/gofiber/fiber/v2"
)

// Auth godoc
//@Summary User email verification for Datasphere panel
//@Descriptiosn User recieves a email with a link including a veryfication token which this endpoints decrypts from AES to plaintext. It includes the user email and how long the request is available
//@Tags auth
//@Produce application/json
//@Param token path string true "token"
//@Router /api/verify/:token [post]
func Verify(c *fiber.Ctx) error {
	token := c.Params("token")

	decryptToken, err := util.AESDecrypt(token)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	splited := strings.Split(decryptToken, ",")
	t, _ := strconv.Atoi(splited[1])

	if t < int(time.Now().Unix()) {
		return c.Status(400).JSON(fiber.Map{
			"error": "Verification email is not valid anymore",
		})
	}

	_, err = database.DB.Exec("UPDATE accounts SET veryfied=1 WHERE email=?", splited[0])
	if err != nil {
		fmt.Print(err)
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	return c.Status(200).JSON(fiber.Map{
		"success": "Sucessfully verified your email",
	})
}
