package services

import (
	"fmt"
	"time"

	"github.com/datasphere/src/database"
	"github.com/datasphere/src/util"
	"github.com/gofiber/fiber/v2"
	"golang.org/x/crypto/bcrypt"
)

type RegisterRequestBody struct {
	Email        string `json:"email"`
	Password     string `json:"password"`
	CaptchaToken string `json:"captchaToken"`
}

// Auth godoc
//@Summary User registration for Datasphere panel
//@Description Alllows the user to register in the Datasphere Panel with email & password. Email needs to be verified after register!
//@Tags auth
//@Accept application/json
//@Produce application/json
//@Param RegisterRequestBody body RegisterRequestBody true "Used to create an account inside Datasphere panel"
//@Router /api/register [post]
func Register(c *fiber.Ctx) error {

	rrb := &RegisterRequestBody{}

	err := c.BodyParser(rrb)
	if err != nil {
		return c.Status(400).JSON(fiber.Map{
			"error": "Invalid request format",
		})
	}

	rows, err := database.DB.Query("SELECT id FROM accounts WHERE email=?", rrb.Email)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	var count int
	for rows.Next() {
		count += 1
	}

	if count != 0 {
		return c.Status(400).JSON(fiber.Map{
			"error": "Account with this email already exists",
		})
	}

	hashedPass, err := bcrypt.GenerateFromPassword([]byte(rrb.Password), 12)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	_, err = database.DB.Exec("INSERT INTO accounts (verified, status, email, password, date) VALUES (?,?,?,?,?);", 0, 1, rrb.Email, hashedPass, time.Now().Unix())
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	emailToken, err := util.AESEncrypt(fmt.Sprintf("%s,%v", rrb.Email, time.Now().Add(time.Hour*24).Unix()))
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	err = util.SendVerificationMail(rrb.Email, emailToken)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	return c.Status(200).JSON(fiber.Map{
		"success": "Sucessfuly created account please verify your email",
	})
}
