package services

import (
	"github.com/datasphere/src/database"
	"github.com/gofiber/fiber/v2"
)

type PricesResponse struct {
	Patcher    int `json:"patcher"`
	Anticheat  int `json:"anticheat"`
	Adminpanel int `json:"adminpanel"`
	Itemshop   int `json:"itemshop"`
	Homepage   int `json:"homepage"`
}

// Price godoc
//@Summary User can get prices for single services from Datasphere panel
//@Description Allows users to fetch the current prices for all services which are offered by Datasphere panel
//@Tags price
//@Produce application/json
//@Router /api/prices [post]
func Prices(c *fiber.Ctx) error {
	pr := &PricesResponse{}

	database.DB.QueryRow("SELECT * FROM pricing;").Scan(&pr.Patcher, &pr.Anticheat, &pr.Adminpanel, &pr.Itemshop, &pr.Homepage)

	return c.Status(200).JSON(pr)
}
