package services

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/datasphere/src/database"
	"github.com/form3tech-oss/jwt-go"
	"github.com/gofiber/fiber/v2"
	"golang.org/x/crypto/bcrypt"
)

type LoginRequestBody struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type AccountLookupResponse struct {
	ID       int
	Email    string
	Password string
	Verify   int
	Status   int
	Balance  int
}

// Auth godoc
//@Summary User login for Datasphere panel
//@Description Allows the user to login into the Datasphere Panel with email & password if email is verified. Auth is using a JWT http only cookie.
//@Tags auth
//@Accept application/json
//@Produce application/json
//@Param LoginRequestBody body LoginRequestBody true "Used to login inside Datasphere Panel"
//@Router /api/login [post]
func Login(c *fiber.Ctx) error {
	lrb := &LoginRequestBody{}

	err := c.BodyParser(lrb)
	if err != nil {
		return c.Status(400).JSON(fiber.Map{
			"error": "Invalid request format",
		})
	}

	res := database.DB.QueryRow("SELECT id, email, password, verified, status, balance FROM accounts WHERE email=?", lrb.Email)

	alr := &AccountLookupResponse{}

	err = res.Scan(&alr.ID, &alr.Email, &alr.Password, &alr.Verify, &alr.Status, &alr.Balance)
	if err != nil {
		fmt.Print(err)
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	err = bcrypt.CompareHashAndPassword([]byte(alr.Password), []byte(lrb.Password))
	if err != nil {
		return c.Status(401).JSON(fiber.Map{
			"error": "Your credentials are wrong or the account does not exsist",
		})
	}

	if alr.Status == 0 {
		return c.Status(400).JSON(fiber.Map{
			"error": "The account has been banned",
		})
	}

	if alr.Verify == 0 {
		return c.Status(400).JSON(fiber.Map{
			"error": "Please verify your email first",
		})
	}

	expirationTime := time.Now().Add(24 * time.Hour)

	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["Id"] = strconv.Itoa(alr.ID)
	claims["Email"] = alr.Email
	claims["Exp"] = expirationTime.Unix()

	tokenStr, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"error": "Internal server error",
		})
	}

	cookie := new(fiber.Cookie)
	cookie.Name = "JWT"
	cookie.Value = tokenStr
	cookie.HTTPOnly = true
	cookie.Expires = expirationTime

	c.JSON(fiber.Map{
		"Id":      alr.ID,
		"Email":   alr.Email,
		"Balance": alr.Balance,
	})
	c.Status(200).Cookie(cookie)

	return nil
}
