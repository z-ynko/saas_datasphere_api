package main

import (
	"os"

	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/datasphere/src/database"
	_ "github.com/datasphere/src/docs"
	"github.com/datasphere/src/services"
	"github.com/gofiber/fiber/v2"
)

//@title Datasphere API
//@version 0.0.1
//@description Welcome Developer to our Game Service API of Datasphere.
//@contact.name Datasphere
//@host localhost:3000
//@BasePath /api
func main() {
	database.OpenDatabaseConnection()
	defer database.DB.Close()

	app := fiber.New()
	api := app.Group("/api")
	if os.Getenv("ENV") == "development" {
		app.Get("/swagger/*", swagger.Handler)
	}

	api.Post("/register", services.Register)
	api.Post("/verify/:token", services.Verify)
	api.Post("/login", services.Login)

	api.Get("/prices", services.Prices)
	app.Listen(":3000")

}
